
public interface Iterator {
	
	boolean hashNext();
	Object next();
	Object currentItem();

}
