
public class OperationList implements List{

	private Operation[] operations;
	
	
	public OperationList(Operation[] operations) {
		this.operations = operations;
	}
	

	@Override
	public Iterator iterator() {
		
		return new OperationIterator(operations);
	}

}
