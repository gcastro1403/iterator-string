
public class RunIterator {

	public static void main(String[] args) {
		Operation[] operations = new Operation[3];
		operations[0] = new Operation(2,(float) 0.2);
		operations[1] = new Operation(1,(float) 0.1);
		operations[2] = new Operation(3,(float) 0.7);
		
		List lista = new OperationList(operations);
		Iterator iterator  = lista.iterator();
		
        while(iterator.hashNext()){
            Operation result = (Operation) iterator.next();
            System.out.println(result.getResult());
        }
        
        float result1 = operations[0].getResult() + operations[1].getResult();
        float totalResult = result1 + operations[2].getResult();
        System.out.println("total "+ totalResult);
        
        //OUTPUT CONSOLA
        //0.4
        //0.1
        //2.1
        //total 2.6

	}

}
