
public class Operation {
	
	private Integer value;
	private float multiplicador;
	private float result;
	


	public Operation(Integer value, float multiplicador) {
		this.value = value;
		this.multiplicador = multiplicador;
	}
	
	
	public Integer getValue() {
		return value;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}
	
	public float getMultiplicador() {
		return multiplicador;
	}
	
	public void setMultiplicador(float multiplicador) {
		this.multiplicador = multiplicador;
	}
	
	public float getResult() {
		return result;
	}

	public void setResult(float result) {
		this.result = result;
	}
	
	
	
}
