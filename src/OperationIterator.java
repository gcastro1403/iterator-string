
public class OperationIterator implements Iterator{
	
	private Operation[] operations;
	private int position;
	
	public OperationIterator(Operation[] operations){
		this.operations = operations;
		this.position = 0;
	}

	@Override
	public boolean hashNext() {
		if(this.position >= operations.length){
            return false;
        }
		float result = operations[position].getValue() * operations[position].getMultiplicador();
		operations[position].setResult(result);
		
        return true;
  
	}

	@Override
	public Object next() {
		 return operations[position++];
	}

	@Override
	public Object currentItem() {
		return operations[position];
	}
	
	
	
}
